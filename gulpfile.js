// Libraries
const gulp = require('gulp');                               // GULP
const sass = require('gulp-sass');							// SASS - SCSS to CSS
const browserSync = require('browser-sync').create();		// Runtime watcher and changer
const clean = require('gulp-clean');						// Cleaner product directory "dev"
const cleanCSS = require('gulp-clean-css');					// CSS minifier
const rigger = require('gulp-rigger');                      // Including some pis of HTML into other HTML включение кусков html в другой
const rename = require("gulp-rename");						// Rename files after minify
const concat = require('gulp-concat');						// Concat for js
const terser = require('gulp-terser');						// Minify for js
const autoprefixer = require('gulp-autoprefixer');			// Cross-browser compatibility css
const imagemin = require('gulp-imagemin');                  // Mini images
const htmlreplace = require('gulp-html-replace');

// BUILD MODE
let buildMode = false;

// File Paths
const fontsFiles = [										// Составляем массив переменних с все файлов шрифтов, для переноса в папку разработки
    './src/fonts/**.eot',
    './src/fonts/**.ttf',
    './src/fonts/**.woff',
    './src/fonts/**.otf'
];

const bootstrapFiles = [
    './src/scss/bootstrap/bootstrap.min.css',
    './src/scss/bootstrap/bootstrap-reboot.min.css'
]

const imgFiles = [
    './src/img/**/*.jpg',
    './src/img/**/*.png'
];

// MODULES
// Cleaning distribution directory
function cleandev() {
    return gulp.src('./dist/', {read: false, allowEmpty: true}).pipe(clean())
}

// Move and minify images
function img() {
    let images = gulp.src(imgFiles);
    if (buildMode) {
        images.pipe(imagemin())
    }
    return images
        .pipe(gulp.dest('./dist/img'))
        .pipe(browserSync.stream());
}

// Copy fonts to distribution directory
function fonts () {
    return gulp.src(fontsFiles)
        .pipe(gulp.dest('./dist/fonts'))
}

// Copy bootstrap css files to distribution directory
function bootstrap () {
    return gulp.src(bootstrapFiles)
        .pipe(gulp.dest('./dist/css/bootstrap'))
}

// Processing scripts
function scripts () {
    let result = gulp.src('src/js/*.js')
        .pipe(concat('script.js'));			                                    // Concat all js files
    if(buildMode) {
        return  result
            .pipe(terser({											                // Minify JS
                toplevel: true
            }))
            .pipe(rename({extname :'.min.js'}))
            .pipe(gulp.dest('./dist/js'));			// Function of rename extname for .js to .min.js
    }
    return result
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream())
}

// Processing HTML
function html() {
    let result = gulp.src("src/*.html")
        .pipe(rigger())
        .pipe(gulp.dest('dist/'));                                         // Contact all lined HTML
    if (buildMode) {
        return  result.pipe(htmlreplace({css: ['./css/style.min.css'], js: ['./js/script.min.js']}))
            .pipe(gulp.dest('./dist/'));
    }
    return result
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.stream());
}

// Processing SCSS
function css() {
    let result = gulp.src('./src/scss/style.scss')
        .pipe(sass({outputStyle: 'expanded'}))
        .pipe(autoprefixer());
    if (buildMode) {
        return result
            .pipe(cleanCSS({level: 2}))
            .pipe(rename({extname :'.min.css'}))
            .pipe(gulp.dest('./dist/css'))
            .pipe(browserSync.stream());
    }
    return result
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
}

// WATCHER
function watch() {
    gulp.src("src/*.html").pipe(htmlreplace({
        css: ['./css/style.css'],
        js: ['./js/script.js']
    })).pipe(gulp.dest('./dist/'));
    html();
    img();
    scripts();
    css();
    bootstrap();
    browserSync.init({											// Tool for Live Reload
        server: {
            baseDir: "./dist/"
        }
    });

    gulp.watch('./src/**/*.scss', css);				      // CSS
    gulp.watch('./src/**/*.js', scripts);                   // JS
    gulp.watch('./src/*.html', html) ;                     // HTML
    gulp.watch('./src/template/*.html', html);             //HTML files in template
    gulp.watch('./src/img/*', img);                       // IMAGES
    gulp.watch('./src/fonts/*', fonts)                    // FONTS
}

// BUILDER
function build(done) {
    buildMode = true;
    html();
    scripts();
    css();
    img();
    fonts();
    bootstrap();
    done();
}

module.exports.watch = watch;
module.exports.build = build;
module.exports.cleandev = cleandev;