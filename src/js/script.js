$('.burger-btn').on('click', function(event) {
    event.preventDefault;
    $('.burger-btn-element').toggleClass('burger-btn-element-active');
});

//price range
$("#filter-price-id").slider({ id: "filterSliderPrice", min: 0, max: 10, range: true, value: [3, 7] });
$("#filter-price-id-xl").slider({ id: "filterSliderPriceXl", min: 0, max: 10, range: true, value: [3, 7] });

//load more pics button in furniture section
const load = document.querySelector('.load-more');

function loadMorePics() {
    const screenWidth = window.innerWidth;
    let furniture;
    let amount;
    if (screenWidth < 768) {
        furniture  = document.querySelectorAll("#product-items > div.furniture-container.d-none");
        amount = 3;
    } else if (screenWidth >= 768 && screenWidth < 1200) {
        furniture = document.querySelectorAll("#product-items > div.furniture-container.d-none:not(.d-md-block)");
        amount = 4;
    } else {
        furniture = document.querySelectorAll("#product-items > div.furniture-container.d-none:not(.d-md-block):not(.d-xl-block)");
        amount = 9;
    }

    if (furniture.length <= amount) {
        amount = furniture.length;
        load.classList.add("d-none");
    }

    for (let i = 0; i < amount; i++) {
        console.log(furniture.item(i))
        furniture.item(i).classList.remove("d-none");
    }
}

load.addEventListener('click', loadMorePics);

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

// Add item to cart

let cartItemArray = [];

$('.button-cart').click(function() {
    event.preventDefault();
    let currentItem = ($(this).closest('.furniture-container'));
    let cartItem = {
        img: currentItem.find('.furniture-block').css('background-image').slice(5, -2),
        name: currentItem.find('.furniture-block-title').html(),
        price: currentItem.find('.furniture-block-price').html(),
        quantity: 1
    };
    cartItemArray.push(cartItem);
    $('.cart-item-qty').html(cartItemArray.length);
    if(cartItemArray.length > 0) {
      $('.cart-item').attr('data-target', '#modal-cart')
    };


    console.log(cartItemArray);
    let wrap = '';
    wrap += '<div>';

    cartItemArray.forEach(function (item, i, cartItemArray) {
        wrap +='<div class="bazar-modal__cart-item container-fluid"><div class="row"><div class="cart-item--img col-6 col-md-4 col-xl-3"><img src="'+item.img+'" alt="" class="img-fluid img-prod"></div><div class="cart-item--price col-6 col-md-6 col-xl-3"><p class="title-prod">'+item.name+'</p><span class="discount-price">$350.00</span><span class="price-modal">'+item.price+'</span></div><div class="cart-item--qty col-8 col-sm-6 col-md-6 col-xl-4 offset-md-4 offset-xl-0"><div>Quantity:</div><input class="quantity" id="quantity" onchange="alert(this.value)" min="1" name="quantity" value="1" type="number"><button onclick="this.parentNode.querySelector(\'.quantity\').stepDown()" class="btn-quantity btn-quantity--minus" >-</button><button onclick="$(this).parent().find(\'input\').value = $(this).parent().find(\'input\').value +1" class=" btn-quantity btn-quantity--plus">+</button></div><div class="cart-item--sum col-4 col-sm-6 col-md-2 col-xl-2 align-items-center">$250.00</div></div></div>';
    });

    wrap += '<div class="bazar-modal-content__control d-flex justify-content-between"><div class="control--left-btn"><a href="#" data-dismiss="modal" aria-label="Close" class="btn-control btn-control--continue">continue</a></div><div class="control--right-btn"><div class="price-cart d-flex">Total: <div class="price-cart-value"> $1000</div></div><a href="#" class="btn-control btn-control--buy">buy now</a></div></div></div>';

    $('#modal-cart').find('.bazar-modal-content').html(wrap);
});










//     $('#modal-cart').find('.bazar-modal-content').prepend('<div class="bazar-modal__cart-item container-fluid">\n' +
//         '                    <div class="row">\n' +
//         '                        <div class="cart-item--img col-6 col-md-4 col-xl-3">\n' +
//         '                            <img src="'+cartItem.img+'" alt="" class="img-fluid img-prod">\n' +
//         '                        </div>\n' +
//         '                        <div class="cart-item--price col-6 col-md-6 col-xl-3">\n' +
//         '                            <p class="title-prod">'+cartItem.name+'</p>\n' +
//         '                            <span class="discount-price">$350.00</span>\n' +
//         '                            <span class="price-modal">'+cartItem.price+'</span>\n' +
//         '                        </div>\n' +
//         '                        <div class="cart-item--qty col-8 col-sm-6 col-md-6 col-xl-4 offset-md-4 offset-xl-0">\n' +
//         '                            <div>Quantity:</div>\n' +
//         '                            <input class="quantity" min="1" name="quantity" value="1" type="number">\n' +
//         '                            <button onclick="this.parentNode.querySelector(\'.quantity\').stepDown()" class="btn-quantity btn-quantity--minus" >-</button>\n' +
//         '                            <button onclick="this.parentNode.querySelector(\'.quantity\').stepUp()" class=" btn-quantity btn-quantity--plus">+</button>\n' +
//         '                        </div>\n' +
//         '                        <div class="cart-item--sum col-4 col-sm-6 col-md-2 col-xl-2 align-items-center">\n' +
//         '                            $250.00\n' +
//         '                        </div>\n' +
//         '                    </div>\n' +
//         '                </div>')